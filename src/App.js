import React from 'react';
import './App.css';
import MainComponent from './components/MainComponent'
import StyleSheet from './components/StyleSheets'

// import Form from './components/Form'

import LifeCycleA from './components/LifeCycleA'
import UserInfo from './components/UserInfo'

import Table from './components/Table'
import PureComp from './components/PureComp'

import ParentComponent from './components/ParentComponent'
import Refs from './components/Refs'
import ClickCounter from './components/ClickCounter'
import FunctionComponent from './mainconcept/FunctionComponent'
import Comment from './mainconcept/Comment'

import LifeCycle from './mainconcept/LifeCycle'
import Check from './mainconcept/Check'
// import Parent from './mainconcept/Parent'
import FormValidate from './mainconcept/Form'
import PropRender from './mainconcept/PropRender'
import Counter from './mainconcept/Counter'
import CounterView from './mainconcept/CounterView'
import MousePointer from './propsrender/MouserPointer'
import UsePointer from './propsrender/UsePointer'

import Sum from './propsrender/Sum'
import Form from './propsrender/Form'
import TextGenerator from './propsrender/TextGenerator'
import TextShow from './propsrender/TextShow'
import TextGenerator2 from './propsrender/TextGEnerator2'
import ComponentA from './Context/ComponentA'
import {ValueProvider} from './Context/ValueContext'
import SumFunction from './Context/SumFunction';
import BookContextProvider from './context1/BookContext'
import BookList from './context1/BookList'

import Parent from './context1/Parent'
import ParentContextProvider from './context1/ParentContext'
import ChildB from './context1/ChildB'

import FormHook from './Hook/Form'

import UseState from './Hook/UseState'

// import Consumer from './Hook/Consumer'

// import ContextProvider from './Hook/Context'

import ContextProvider from './hookcontext/Context'

import Consumer from './hookcontext/Consumer'

import MyForm from './HOC/MyForm'

import HomePage from './HOC/HomePage'

import StateComponent from './functional_component/StateComponent'

function App() {
  return (

    <div className="App">
      {/* <MainComponent/> */}
      {/* <StyleSheet/> */}
      {/* <Form/> */}
      {/* <LifeCycleA/> */}
      {/* <UserInfo/> */}
      {/* <Table/> */}
      {/* <PureComp/> */}
      {/* <ParentComponent/> */}
    {/* <Refs age="30"/> */}

    {/* <ClickCounter/> */}
    {/* <ElementCounter/> */}
    {/* <FunctionComponent name="Function" type='Functional Component'/> */}

    {/* <Comment />  */}
    {/* <LifeCycle/> */}
    {/* <Check/> */}
    {/* <Parent loggedin={true}/> */}
    {/* <PropRender render={(printname)=>printname?"Nahid":""}/> */}

      {/* <Counter render={(counter,incrmentCounter)=> <CounterView counter={counter} incrmentCounter={incrmentCounter}/>}/> */}

    {/* <FormValidate/> */}
    {/* <MousePointer render={(x,y,pointerPos)=> <UsePointer x={x} y={y} pointerPos={pointerPos} /> }/> */}
    {/* <Sum render={(v1,v2,sum,getValue,calcSum)=> <Form v1={v1} v2={v2} sum={sum} getValue={getValue} calcSum={calcSum} />}  /> */}

    {/* <TextGenerator render={(text,generateText)=>(<TextShow text={text}  generateText={generateText}/>)}/> */}
    {/* <TextGenerator render={(text,generateText)=>(<TextGenerator2 text={text} generateText={generateText}/> )}/> */}

  {/* <ValueProvider value={"Context Preview"}>

      <SumFunction render={(val1,val2,sum,valueHandler,calculateDum)=>(<ComponentA val1={val1} val2={val2} sum={sum} valueHandler={valueHandler} calculateDum={calculateDum}/>)}  />

  </ValueProvider> */}
{/* 
  <BookContextProvider>
    <BookList/>
  </BookContextProvider> */}

    {/* <ParentContextProvider>
      <Parent></Parent>
    </ParentContextProvider> */}

    {/* <FormHook/> */}

    {/* <UseState/> */}

  {/* <ContextProvider>
      <Consumer/>
  </ContextProvider> */}

  {/* <ContextProvider>
    <Consumer/>
  </ContextProvider> */}

  {/* <MyForm/> */}
  {/* <HomePage/> */}
    <StateComponent/>

    </div>
  );
}

export default App;
