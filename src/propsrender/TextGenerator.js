import React, { Component } from 'react'

class TextGenerator extends Component {

    constructor(props)
    {
        super(props)
        this.state={
            text:''
        }
    }

    generateText=(e)=>{
        this.setState({text:e.target.value})
    }

    render() {
        return (
            <div>
                {this.props.render(this.state.text,this.generateText)}
            </div>
        )
    }
}

export default TextGenerator
