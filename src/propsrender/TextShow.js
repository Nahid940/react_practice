import React, { Component } from 'react'

class TextShow extends Component {
    render() {
        const {text,generateText}=this.props
        return (
            <div>
                <input type="text" name="text" onChange={generateText}/>
                Text you have typed is {text} 
                
            </div>
        )
    }
}

export default TextShow
