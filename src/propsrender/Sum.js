import React, { Component } from 'react'

class Sum extends Component {

    constructor(props)
    {
        super(props)
        this.state={
            v1:0,
            v2:0,
            sum:0
        }
    }

    getValue=(e)=>{
        this.setState({ [e.target.name]: e.target.value });
    }

    calcSum=()=>{
        this.setState({
            sum:parseInt(this.state.v1)+parseInt(this.state.v2)
        })
    }


    render() {
        const{v1,v2,sum}=this.state
        return (
            <div>
                {this.props.render(v1,v2,sum,this.getValue,this.calcSum)}
            </div>
        )
    }
}

export default Sum
