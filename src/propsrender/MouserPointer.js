import React, { Component } from 'react'

class MouserPointer extends Component {

    constructor(props)
    {
        super(props)
        this.state={
            x:0,
            y:0
        }
    }

    pointerPos=(e) =>{
        this.setState({
            x:e.clientX,
            y:e.clicentY
        })
    }

    render() {
        return (
            <div onMouseMove={this.pointerPos}>
                {this.props.render(this.state.x,this.state.y,this.pointerPos)}
            </div>
        )
    }
}

export default MouserPointer
