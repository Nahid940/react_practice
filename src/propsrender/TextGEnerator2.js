import React, { Component } from 'react'
class TextGEnerator2 extends Component {
    
    render() {
        const {text,generateText}=this.props
        return (
            <div>

                <input type="text" name="text" onChange={generateText}/>

                <h3>This is from Text generator 2 and the text is {text}</h3>
                
            </div>
        )
    }
}

export default TextGEnerator2
