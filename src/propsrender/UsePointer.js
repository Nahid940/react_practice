import React, { Component } from 'react'

class UsePointer extends Component {
    render() {
        const {x,y,pointerPos}=this.props
        return (
            <div onMouseMove={pointerPos}>
                <p>This  x position is {x} and y positon is {y}</p>
            </div>
        )
    }
}

export default UsePointer
