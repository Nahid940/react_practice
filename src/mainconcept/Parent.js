import React, { Component } from 'react'
import Greeting from './Greeting'
import Message from './Message'

class Parent extends Component {

    constructor(props)
    {

        super(props)

        this.state={
            loggedin:false
        }


    }

    clickHandler=()=>
    {
        this.setState({
                loggedin:true
            })
    }

    render() {

       //const {loggedin}=this.props

        if(this.state.loggedin==true)
        {
            return (
                <div>
                    <Greeting/>
                </div>
            )
        }else
        {
           return (
                <div>
                    <Message/>
                    <button onClick={this.clickHandler}>Click here to get access</button>
                </div>
           )
        }


        
    }
}

export default Parent
