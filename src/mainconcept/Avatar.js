import React from 'react'

function Avatar(props)
{
    const {src,alt}=props
    return (
        
        <div>
            <img src={src} alt={alt}/>
        </div>
    )
}
export default Avatar