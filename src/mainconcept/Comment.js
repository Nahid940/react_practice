import React from 'react'
import UserInfo from './UserInfo'
import TimeTable from './TimeTable'


function Comment(props)
{
    const {comment,author}=props;

    const comment_attrs={
        comment:'Hello dear',
        author:"Nahid"
    };

    return (
        <div>
            <UserInfo name="Nahid"/>
            Commende<TimeTable time_value={new Date().toLocaleTimeString()}/>
            <p>The comment is {comment_attrs.comment} posted by {comment_attrs.author}</p>
        </div>
    )
}

export default Comment