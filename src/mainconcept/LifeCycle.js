import React from 'react'

class LifeCycle extends React.Component
{

    constructor(props)
    {
        super(props)

        this.state={

            timeState:new Date()
        }
    }

    componentDidMount()
    {
        this.myTime=setInterval(
            ()=>this.tick(),1000
        );

        console.log("Will Mounted")
    }

    componentWillUnmount()
    {
        console.log("Component removed")
        clearInterval(this.myTime)
    }

    tick() {
        this.setState({
            timeState:new Date()
        })
    }


    render()
    {
        return(
            <div>
                <p>The time is {this.state.timeState.toLocaleTimeString()}</p>
                {console.log(this.myTime)}
            </div>
        )
    }

    componentWillMount()
    {
        console.log("Will Mount")
    }
}

export default LifeCycle