import React, { Component } from 'react'

class Counter extends Component {

    constructor(props)
    {
        super(props)
        this.state={
            counter:0
        }

        this.counterHandler=this.counterHandler.bind(this)

    }

    counterHandler() {
        this.setState(prevState=>{
            return {counter:prevState.counter+1}
        })
    }
    // counterHandler() {
    //     this.setState({
    //         counter:this.state.counter+1
    //     })
    // }

    render() {
        return (
            <div>
                {this.props.render(this.state.counter,this.counterHandler)}
            </div>
        )
    }
}

export default Counter
