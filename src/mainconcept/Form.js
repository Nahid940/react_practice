import React, { Component } from 'react'

class Form extends Component {

    constructor(props)
    {
        super(props)

        this.state={
            name:'',
            email:'',

            formErroros:{
                name:'',
                email:''
            }
        }
    }

    submitHandler=(e)=>
    {
        
        console.log(Object.values())

    }

    handleChange=(e)=>{
        e.preventDefault();
        const{name,value}=e.target

        let formerros=this.state.formErroros
        // console.log(value)


        switch(name)
        {
            case 'name':
            formerros.name=value.length<2 ?"Name should be more than 2 characters":"";
            break;

            case 'email':
            formerros.email=value.length<5?"Email must be 5 character long":"";
            break;
        }

        // console.log(formerros)

        this.setState({formerros,[name]:value},()=>console.log(this.state))
    }

    render() {
        return (
            <div>

                <label htmlFor="">Name</label>
                <input type="text" name="name" id="name"  onChange={this.handleChange}/>
                <input type="text" name="email" id="email"  onChange={this.handleChange}/>
                <button onClick={this.submitHandler}>Click</button>
                
            </div>
        )
    }
}

export default Form
