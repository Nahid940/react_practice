import React from 'react'
function FunctionComponent(props)
{
    const {name,type}=props

    return(
        <div>
            <h1>Function {name} and Type is {type}</h1>
        </div>
    )
}

export default FunctionComponent