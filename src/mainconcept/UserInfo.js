import React from 'react'
import Avatar from './Avatar'

function UserInfo(props)
{
    return (
        <div>
            <Avatar src="https://cdn1.iconfinder.com/data/icons/user-pictures/100/male3-512.png" alt="Avatar Image"/>
            Hello my name is <h1>{props.name}</h1>
        </div>
    )
}

export default UserInfo