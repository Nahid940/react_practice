import React, { Component } from 'react'

class CounterView extends Component {
    render() {
        const {counter,incrmentCounter}=this.props
        return (
            <div>

                <button onClick={incrmentCounter}>Clicked {counter} time</button>

            </div>
        )
    }
}

export default CounterView
