import React,{createContext,useState} from 'react'

export const TextContext=createContext()

const ContextProvider=(props)=>{

    const [value,setValue]=useState({
        value:1000,
        title:'This is from context'
    })

    return (
        <TextContext.Provider value={{value}}>
            {props.children}
        </TextContext.Provider>
    )
}

export default ContextProvider