import React ,{useState} from 'react'

const UseState=(props)=>{

    const [mySeris, setMySeris]=useState([
        {
            id:5,
            name:"john"
        },
        {
            id:6,
            name:"johny"
        }
    ])

    const [x,setX]=useState(0)

    const changeX=()=>{
        setX(x+1)
    }

    const [seris,setSeris]=useState({id:'',name:''})

    const inputChange=(event)=>{
        setSeris({...seris,[event.target.name]:event.target.value})
    }


    const myArray=[4,234,43,5,435,4,35,43,5]

    const submitForm=(event)=>{
        event.preventDefault()
        
        const submitValue={
            id:seris.id,
            name:seris.name
        }
        setMySeris([...mySeris,submitValue])

        console.log([...myArray,1000])
    }

    return (
        <div>

            <button onClick={changeX}>You have clicked {x} times</button>

            <form onSubmit={submitForm}>
                <table>
                    <tr>
                        <td><input type="text" name="id" value={seris.id}  onChange={inputChange}/></td>
                        <td><input type="text" name="name" value={seris.name}  onChange={inputChange}/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button>Submit</button></td>
                    </tr>
                </table>
            </form>

            <table style={{width:'50%',color:'green',border:'1px solid black'}}>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                    </tr>
                </thead>

                <tbody>
                    {mySeris.map(ms=>(
                        <tr>{ms.id}
                            <td>{ms.name}</td>
                        </tr>
                    ) ) }
                </tbody>
            </table>
        </div>
    )
}

export default UseState