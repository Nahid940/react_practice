import React,{useState} from 'react'


const FormHook=()=>
{
    const [person,setPerson]=useState([
        {
            name:"Nahid",
            age:20
        },
        {
            name:'Shiblu',
            age:35
        }
    ])

    const [newperson,setNewperson]=useState({name:'',age:''})

    const onChangeForm=(event)=>{
        setNewperson({...newperson,[event.target.name]:event.target.value})
    }

    const onFormSubmit=(event)=>{
        event.preventDefault()

        const newPerson={
            name:newperson.name.trim(),
            age:newperson.age.trim()
        }
        // console.log(newPerson)
        setPerson([...person,newPerson])

        newperson.name='';
        newperson.age=''
    }

    return (
        <div>
            <form onSubmit={onFormSubmit}>
                <input type="text" name="name" value={newperson.name} onChange={onChangeForm}/>
                <input type="text" name="age" value={newperson.age} onChange={onChangeForm}/>
                <button type='submit'>Add</button>
            </form>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Age</th>
                    </tr>
                </thead>

                <tbody>
                    {person.map(p=>(
                        <tr key={Math.random()*1000}>
                            <td>{p.name}</td>
                            <td>{p.age}</td>
                        </tr>
                    ) ) }
                </tbody>
            </table>
        </div>
    )
}

export default FormHook