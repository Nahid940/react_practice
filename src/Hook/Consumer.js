import React,{useContext} from 'react'
import {TextContext} from './Context'

const Consumer=()=>{
    const {value}=useContext(TextContext)

    return (
        <div>
            The Value is {value.title}
        </div>
    )
}

export default Consumer