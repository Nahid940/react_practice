import React, { Component } from 'react'

class LifeCycleA extends Component {

    //Moutung Phase
    constructor(props)
    {
        super(props)
        this.state={

            name:''
        }

        console.log("constructor")
    }

    static getDerivedStateFromProps(props,state)
    {
        console.log("get Derived State From Props")
        return null
    }

    componentDidMount()
    {
        console.log("component did mount")
    }

    //updateing phase
    shouldComponentUpdate()
    {
        console.log("component shouldComponentUpdate")
        return true
    }

    getSnapshotBeforeUpdate(previousState,previousProps)
    {
        console.log("component getSnapshotBeforeUpdate")
        return null
    }

    componentDidUpdate(previousState,previousProps,snapshot)
    {
        console.log("component componentDidUpdate")
    }


    ChnangeState=()=>
    {
        this.setState({
            name:'Nahid'
        })
    }


    render() {
        console.log("render")
        return (
            <div>

                <button onClick={this.ChnangeState}>Click Me</button>
                <p>Name is {this.state.name}</p>
                
            </div>
        )
    }
}

export default LifeCycleA
