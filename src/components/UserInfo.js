import React, { Component } from 'react'

class UserInfo extends Component {

    constructor(props)
    {
        super(props)

        this.state={
            name:''
        }

        this.getName=this.getName.bind(this)
    }

    getName(event)
    {
        this.setState({
            name:event.target.value
        })
    }

    // getName=(event)=>{
        
    // }


    render() {
        return (
            <div>

                <input type="text" value={this.state.name} onChange={this.getName}/>

                <p>This Typed Name is {this.state.name}</p>
                
            </div>
        )
    }
}

export default UserInfo
