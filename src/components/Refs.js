import React, { Component } from 'react'

class Refs extends Component {

    constructor(props)
    {
        super(props)
        this.x_ref=React.createRef()
        // this.clickHandlers=this.clickHandlers.bind(this)

        this.state={
            name:null,
            age:null
        }

        this.my_ref=React.createRef()
    }

    componentDidMount()
    {
        // console.log(this.x_ref)
        //this.x_ref.current.focus()
    }
    clickHandlers()
    {
        alert(this.my_ref.current.value)
    }

    updateHandler=()=>{
        this.setState({
            name:this.my_ref.current.value
        })
    }
    
    componentDidUpdate(prevProps,prevState)
    {
        if(this.state.name!==prevState.name)
        {
            alert(this.state.name)
        }
    }

    render() {
        const {age}=this.props
        return (
            <div>
                {/* <input type="text" ref={this.x_ref}/>
                <button onClick={this.clickHandlers}>Click</button> */}
                <input type="text" ref={this.my_ref} value={age}/>
                <button onClick={this.updateHandler}>Update the value</button>

                
                <p>The text is {this.TextHover}</p>

            </div>
        )
    }
}

export default Refs
