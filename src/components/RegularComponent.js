import React, { PureComponent } from 'react'

class RegularComponent extends PureComponent {

    render() {
        console.log("Regular component")
        return (
            <div>
                <p>Name is {this.props.name}</p>
            </div>
        )
    }
}

export default RegularComponent
