import React from 'react'
import TableContent from './TableContent'

function Table() {
    return (
        <table>
            <tbody>
                <tr>
                    <TableContent/>
                </tr>
            </tbody>
        </table>
    )
}

export default Table
