import React, { Component } from 'react'
import ChildComponent from './ChildComponent'

class MainComponent extends Component {


    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }

    render() {

        const myList=[
            {
                id:1,
                name:'Nahid',
                age:30
            },
            {
                id:2,
                name:'Akib',
                age:25
            },
            {
                id:3,
                name:'Shiblu',
                age:35
            },
        ];

        return (
            <div>

                 {myList.map(ml=>(<ChildComponent  personLists={ml} />))}

            </div>
        )
    }
}

export default MainComponent
