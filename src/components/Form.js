import React, { Component } from 'react'

class Form extends Component {

    constructor(props)
    {

        super(props)

        this.state={
            username:'',
            comments:'',
            age:45
        }
    }

    handeUserName=(event)=>{
        this.setState({
            username:event.target.value
        })
    }
    
    handleComment=(event)=>{
        this.setState({
            comments:event.target.value
        })
    }

    handleAge=(event)=>{
        this.setState({
            age:event.target.value
        })
    }

    submitForm=(event)=>{
        
        alert(` Name is ${this.state.name} and comment is ${this.state.comments} and age is ${this.state.age}`)

        event.preventDefault()

    }

    render() {
        return (
         
            <form onSubmit={this.submitForm}>

                <div>
                    <label htmlFor="">Name</label>
                    <input type="text" value={this.state.username}  onChange={this.handeUserName}/>
                </div>

                <div>
                    <label htmlFor="">Comment</label>
                    <textarea name="" value={this.state.comments} onChange={this.handleComment} id="" cols="30" rows="10"></textarea>
                </div>


                <div>
                    <label htmlFor="">Select Age</label>

                    <select name="" id="" value={this.state.age} onChange={this.handleAge}>
                        <option value="30">30</option>
                        <option value="45">45</option>
                        <option value="60">60</option>
                    </select>
                </div>

                <button type="submit">Submit</button>

            </form>
                
          
        )
    }
}

export default Form
