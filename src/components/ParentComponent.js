import React, { Component } from 'react'
import RegularComponent from './RegularComponent'

import PureComp from './PureComp'

class ParentComponent extends Component {

    constructor(props)
    {
        super(props)

        this.state={
            name:'Nahid'
        }
    }

    componentDidMount()
    {
        setInterval(()=>{
            this.setState({
                name:"Nahid"
            })
        },2000)
    }

    render() {
        console.log("ok")
        return (
            <div>

                <RegularComponent name={this.state.name}/>
                <PureComp name={this.state.name}/>
            </div>
        )
    }
}

export default ParentComponent
