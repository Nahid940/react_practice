import React, { PureComponent } from 'react'

class PureComp extends PureComponent {
    render() {
        console.log("Pure component")
        return (
            <div>
                <p>Name from pure comp is {this.props.name}</p>
            </div>
        )
    }
}

export default PureComp
