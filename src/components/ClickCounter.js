import React, { Component } from 'react'
import UpdatedComponent from './WithCounter'


class ClickCounter extends Component {
    render() {
        const {counter,clickHandler}=this.props
        return (
            <div>
                <p>Clicked {counter} times</p>
                <button onClick={clickHandler}>Click Me  times</button>
                
            </div>
        )
    }
}

export default UpdatedComponent(ClickCounter)
