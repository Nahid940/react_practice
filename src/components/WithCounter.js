
import React from 'react'

const  UpdatedComponent=OriginalComponent=>{

    class NewComponent extends React.Component
    {
        constructor(props)
        {
            super(props)
            this.state={
                counter:0
            }
        }

        clickHandler=()=>{
            // this.setState(prevState=>{
            //     counter:prevState.counter+1
            // })

            this.setState({
                counter:this.state.counter+1
            })
        }
        
        render()
        {
            return <OriginalComponent counter={this.state.counter} clickHandler={this.clickHandler}/>
        }
    }
    return NewComponent
}

export default UpdatedComponent
