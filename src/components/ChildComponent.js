import React, { Component } from 'react'

class ChildComponent extends Component {
    render() {
        return (
            <div>
                <h1>Person Name is {this.props.personLists.name}</h1> and <h1>Age is {this.props.personLists.age}</h1>
            </div>
        )
    }
}
export default ChildComponent
