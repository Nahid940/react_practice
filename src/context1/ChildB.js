import React, { Component ,useContext} from 'react'
import {PrentContext} from './ParentContext'

const ChildB=(props)=>{
    const {person}=useContext(PrentContext)

    return (
        <div>
            {person.name}
            {person.age}
            {person.address} 
        </div>
    )
}

export default ChildB
