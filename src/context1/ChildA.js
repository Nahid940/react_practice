import React, { Component } from 'react'
import ChildB from './ChildB'
class ChildA extends Component {

    constructor(props)
    {
        super(props)
    }

    render() {
        return (
            <div>

                A
                ---------------------------------
                {this.props.children}
                <ChildB/>
                
            </div>
        )
    }
}

export default ChildA
