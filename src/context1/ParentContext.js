import React,{useState,createContext} from 'react'

export const PrentContext =createContext()

const ParentContextProvider=(props)=>{

    const [person,setPerson]=useState(
        {
            name:"Nahid",
            age:20,
            address:"Dhaka"
        }
    )

    return (
        <PrentContext.Provider value={{person}}>
            {props.children}
        </PrentContext.Provider>
    )
}
export default ParentContextProvider