import React,{ Component }  from 'react'

const HoverText=(OriginalComponent)=>
{
    class NewComponent extends Component {


        constructor(props)
        {
            super(props)
            this.state={
                text:""
            }
        }

        onHover=()=>{
            this.setState({
                text:"You have hovered over the text"
            })
        }

        render() {
            const {text}=this.state
            return (
                <OriginalComponent text={text} onHover={this.onHover}/>
            )
        }
    }
    return NewComponent
}

export default HoverText
