import React,{useState} from 'react'
import FormDataCompoenent from './FormDataComponent'

import ShowHoverText from './ShowHoverText'

const StateComponent=()=>{

    const [mydata,setMyData]=useState({
        name:"",
        age:"",
        email:""
    })

    const [showFormData,setShowFormData]=useState(false)
  

    const changeHandler=(event)=>
    {
        setMyData({
            ...mydata,[event.target.name]:event.target.value
        })
    }

    const submitHandler=(event)=>{
        event.preventDefault()
        if(mydata.name!='' && mydata.age!='' && mydata.email!='')
        {
            setShowFormData({
                showFormData:true
            })
        }
    }

    var FormDataCompoenents='';

    if(showFormData)
    {
        FormDataCompoenents=<FormDataCompoenent name={mydata.name} age={mydata.age} email={mydata.email}/>;
    }else
    {
        FormDataCompoenents=''
    }

    return (
        <div>
            <form onSubmit={submitHandler}>
                <input type="text" placeholder="Name" value={mydata.name} onChange={changeHandler} name="name"/>
                <input type="text" placeholder="Age" value={mydata.age} onChange={changeHandler} name="age"/>
                <input type="text" placeholder="Email" value={mydata.email} onChange={changeHandler} name="email"/>
                <button type="submit">Save</button>
            </form>
            {FormDataCompoenents}

            <ShowHoverText/>
        </div>
    )
    
}
export default StateComponent