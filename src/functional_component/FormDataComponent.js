import React from 'react'


const FormDataComponent=(props)=>{

    const {name,age,email}=props

    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Age</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{name}</td>
                        <td>{email}</td>
                        <td>{age}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}
export default FormDataComponent