import React from 'react'
import HoverText from './HoverText'
const ShowHoverText=(props)=>{
    return (
        <div>
            <p onMouseMove={props.onHover}> Hello {props.text}</p>
        </div>
    )
}

export default HoverText(ShowHoverText)