import React,{createContext,useState} from 'react'

export const Context=createContext();

const ContextProvider=(props)=>{

    const [students,setStudent]=useState([
        {
            name:"Akash",
            age:35
        },
        {
            name:"Jam",
            age:40
        },
        {
            name:"Kab",
            age:40
        },
        {
            name:"Latin",
            age:25
        }
    ])

    return (
        <Context.Provider value={{students}}>

            {props.children}

        </Context.Provider>
    )
}
export default ContextProvider