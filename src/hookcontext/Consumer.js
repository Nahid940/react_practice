import React,{useContext,useState} from 'react'
import {Context} from './Context'

const Consumer=()=>{
    const {students}=useContext(Context)

    const [newStudents,setNewStudent]=useState({name:'New Sudent',age:30})

    // setNewStudent({...newStudents,students})

    return (
        <div>
            {students.map(student=>(
                <React.Fragment>
                    <li>{student.name}</li>
                    <li>{student.age}</li>
                </React.Fragment>
             ) ) }
        </div>
    )
}

export default Consumer