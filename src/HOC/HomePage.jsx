import React,{useState} from 'react'
import MyText from './Text'

function HomePage(props) {

    return (
        <div>

            <p onMouseMove={props.onHoverHandler}>Hello {props.username}</p>

        </div>
    )
}

export default MyText(HomePage)
