import React,{Component} from 'react'

const MyText=(OldComponent)=>{

    class NewText extends Component {

        constructor(props)
        {
            super(props)
            this.state={
                username:''
            }
        }

        onHoverHandler=()=>{
            this.setState({
                username:"Nahid"
            })
        }


        render() {
            const {username}=this.state
            return (
                <OldComponent username={username} onHoverHandler={this.onHoverHandler}></OldComponent>
            )
        }
    }
    return NewText
}
export default MyText