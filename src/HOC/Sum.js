import React,{Component} from'react'

const DoSumComponent=(MyComponent)=>{

    class NewComponent extends Component
    {
        constructor(props)
        {
            super(props)

            this.state={
                x:0,
                y:0,
                sum:0
            }
        }

        inputHandler=(event)=>{
            this.setState({
                [event.target.name]:event.target.value
            })
        }
        sumHandler=(event)=>{
            this.setState({
                sum:Number(this.state.x)+Number(this.state.y)
            })
        }
        render()
        {
            const {x,y,sum}=this.state
            return (<MyComponent x={x} y={y} sum={sum} inputHandler={this.inputHandler} sumHandler={this.sumHandler} />)
        }
    }
    return NewComponent
}
export default DoSumComponent