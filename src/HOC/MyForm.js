import React, { Component } from 'react'
import DoSumComponent from './Sum'

class MyForm extends Component {
    render() {
        const {x,y,sum,inputHandler,sumHandler}=this.props
        return (
            <div>
                <input type="text" name="x" onChange={inputHandler}/>
                <input type="text" name="y" onChange={inputHandler}/>

                
                <button onClick={sumHandler}>Do Sum</button>

                <p>The value is {sum}</p>
            </div>
        )
    }
}

export default DoSumComponent(MyForm)
