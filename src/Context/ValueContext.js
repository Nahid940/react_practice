import React from 'react'

const ValueContext =React.createContext()

const ValueProvider=ValueContext.Provider;
const ValueConsumer=ValueContext.Consumer

export {ValueProvider,ValueConsumer}