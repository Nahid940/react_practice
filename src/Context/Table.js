import React from 'react'

function Table(props) {
    return (
        <div>
            <table style={{width:'50%',border:'1px solid black',padding:'5px'}}>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Context</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td>This is {props.name}</td>
                        <td>Context is {props.context}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

export default Table
