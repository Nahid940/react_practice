import React, { Component } from 'react'
import {ValueConsumer} from './ValueContext'
import Table from './Table'

class ComponentC extends Component {
    render() {
        return (
            <ValueConsumer>
                {
                    (context)=>{
                        return (
                            <div>
                                <Table name={"Compoent C"} context={context}/>
                            </div>
                        )
                    }
                }
            </ValueConsumer>
        )
    }
}

export default ComponentC
