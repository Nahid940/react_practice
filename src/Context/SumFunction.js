import React, { Component } from 'react'

 class SumFunction extends Component {

    constructor(props)
    {
        super(props)

        this.state={
            val1:0,
            val2:0,
            sum:0
        }
    }

    valueHandler=(e)=>{
        this.setState({
            [e.target.name]:e.target.value
        })
    }

    calculateSum=()=>{
        this.setState({
            sum:Number(this.state.val1)+Number(this.state.val2)
        })
    }

    render() {
        const {val1,val2,sum}=this.state
        return (
            <div>
                {this.props.render(val1,val2,sum,this.valueHandler,this.calculateSum)}
            </div>
        )
    }
}

export default SumFunction
