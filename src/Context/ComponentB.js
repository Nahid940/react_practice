import React, { Component } from 'react'
import ComponentC from './ComponentC'

class ComponentB extends Component {
    render() {
        return (
            <div>
                This is component B
                <ComponentC/>
            </div>
        )
    }
}

export default ComponentB
