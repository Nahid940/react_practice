import React, { Component } from 'react'
import ComponentB from './ComponentB'
import Table from './Table'

import {ValueConsumer} from './ValueContext'

class ComponentA extends Component {
    render() {
        const {val1,val2,sum,valueHandler,calculateDum}=this.props
        return (

            <ValueConsumer>
                {
                    (ContextName)=>{

                        return (
                            <div>
                                <Table name={"Component A"} context={ContextName} />
                                <ComponentB/>

                                <input type="text" name="val1" onChange={valueHandler}/>
                                <input type="text" name="val2" onChange={valueHandler}/>
                                <button onClick={calculateDum}> Click</button>

                                The valu is {sum}


                            </div>
                        )
                    }
                }
            </ValueConsumer>


            
        )
    }
}

export default ComponentA
